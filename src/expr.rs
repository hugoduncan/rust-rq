//! Expression nodes.

use ::error::*;

/// An expression to return a field in a map, or
/// an entry in an associative data structure.
#[derive(Clone, Debug, PartialEq)]
pub struct FieldExpr(pub Key);

/// An expression to return an element from an
/// indexed data structure.
#[derive(Clone, Debug, PartialEq)]
pub struct IndexExpr(pub usize);

/// An expression to return the whole of a data structure.
#[derive(Clone, Debug, PartialEq)]
pub struct IdentityExpr;

/// An intermediate expression.
#[derive(Clone, Debug, PartialEq)]
pub enum SubExpr{
    Field(FieldExpr),
    Index(IndexExpr),
    Identity(IdentityExpr),
}

/// An expression is a sequence of piped sub expressions
#[derive(Clone, Debug, PartialEq)]
pub struct Expr(pub Vec<SubExpr>);
