//! Filtering of Rust data structures
//!
//! Allow runtime querying of nested rust data structures.
//! Inspired by jq.

use std::collections::HashMap;
use std::convert::Into;

use serde::ser;

use ::error::*;
use ::expr::*;

/// Map type
pub type Map<V> = HashMap<String,V>;

/// Filter serializer
struct Serializer {
    expr: Vec<SubExpr>,
    value: Option<String>,
}

impl Serializer {
    /// Return a new serializer for expr
    fn new(expr: Expr) -> Serializer {
        Serializer {
            expr: expr.0.into_iter().rev().collect(),
            value: None
        }
    }

    fn expr_head(&mut self) -> (SubExpr, bool)
    {
        (self.expr.pop().unwrap(), self.expr.is_empty())
    }
}

impl ser::Serializer for Serializer {
    type Error = Error;


    #[inline]
    fn visit_bool(&mut self, _value: bool) -> Result<(), Error> {
        unimplemented!();
    }

    #[inline]
    fn visit_isize(&mut self, _value: isize) -> Result<(), Error> {
        unimplemented!();
    }

    #[inline]
    fn visit_i8(&mut self, _value: i8) -> Result<(), Error> {
        unimplemented!();
    }

    #[inline]
    fn visit_i16(&mut self, _value: i16) -> Result<(), Error> {
        unimplemented!();
    }

    #[inline]
    fn visit_i32(&mut self, _value: i32) -> Result<(), Error> {
        unimplemented!();
    }

    #[inline]
    fn visit_i64(&mut self, _value: i64) -> Result<(), Error> {
        unimplemented!();
    }

    #[inline]
    fn visit_usize(&mut self, _value: usize) -> Result<(), Error> {
        unimplemented!();
    }

    #[inline]
    fn visit_u8(&mut self, _value: u8) -> Result<(), Error> {
        unimplemented!();
    }

    #[inline]
    fn visit_u16(&mut self, _value: u16) -> Result<(), Error> {
        unimplemented!();
    }

    #[inline]
    fn visit_u32(&mut self, value: u32) -> Result<(), Error> {
        let (expr, leaf) = self.expr_head();
        match expr {
            SubExpr::Field(FieldExpr(_)) => Err(Error::NotAssociative),
            SubExpr::Index(_) => Err(Error::NotAnArray),
            SubExpr::Identity(_) => {
                if leaf {
                    self.value = Some(value.to_string());
                    Ok(())
                } else {
                    self.visit_u32(value)
                }
            }
        }
    }

    #[inline]
    fn visit_u64(&mut self, _value: u64) -> Result<(), Error> {
        unimplemented!();
    }

    #[inline]
    fn visit_f32(&mut self, _value: f32) -> Result<(), Error> {
        unimplemented!();
    }

    #[inline]
    fn visit_f64(&mut self, _value: f64) -> Result<(), Error> {
        unimplemented!();
    }

    #[inline]
    fn visit_char(&mut self, _value: char) -> Result<(), Error> {
        unimplemented!();
    }

    #[inline]
    fn visit_str(&mut self, value: &str) -> Result<(), Error> {
        let (expr, leaf) = self.expr_head();
        match expr {
            SubExpr::Field(FieldExpr(_)) => Err(Error::NotAssociative),
            SubExpr::Index(_) => Err(Error::NotAnArray),
            SubExpr::Identity(_) => {
                if leaf {
                    self.value = Some(value.into());
                    Ok(())
                } else {
                    self.visit_str(value)
                }
            }
        }
    }

    #[inline]
    fn visit_none(&mut self) -> Result<(), Error> {
        self.value = Some("None".to_string());
        Ok(())
    }

    #[inline]
    fn visit_some<V>(&mut self, value: V) -> Result<(), Error>
        where V: ser::Serialize
    {
        value.serialize(self)
    }

    #[inline]
    fn visit_unit(&mut self) -> Result<(), Error> {
        unimplemented!();
    }

    #[inline]
    fn visit_enum_unit(&mut self, _name: &str, _variant: &str) -> Result<(), Error> {
        unimplemented!();
    }

    #[inline]
    fn visit_seq<V>(&mut self, _visitor: V) -> Result<(), Error>
        where V: ser::SeqVisitor,
    {
        unimplemented!();
    }

    #[inline]
    fn visit_enum_seq<V>(&mut self, _name: &str, _variant: &str, _visitor: V) -> Result<(), Error>
        where V: ser::SeqVisitor,
    {
        unimplemented!();
    }

    #[inline]
    fn visit_seq_elt<T>(&mut self, _value: T) -> Result<(), Error>
        where T: ser::Serialize,
    {
        unimplemented!();
    }

    #[inline]
    fn visit_map<V>(&mut self, mut visitor: V) -> Result<(), Error>
        where V: ser::MapVisitor,
    {
        let leaf = self.expr.len() == 1;
        let head = self.expr.last().unwrap().clone();

        match head {
            SubExpr::Field(FieldExpr(_)) => {
                while let Some(()) = try!(visitor.visit(self)) { };
                Ok(())
            },
            SubExpr::Index(_) => Err(Error::NotAnArray),
            SubExpr::Identity(_) => {
                if leaf {
                    // We need the map value.
                    // self.value = Some(Value::Val(Box::new(value)));
                    // We could alternative return a Map<>
                    Err(Error::InternalError)
                } else {
                    // visit for the next expression
                    self.expr.pop();
                    self.visit_map(visitor)
                }
            }
        }
    }

    #[inline]
    fn visit_enum_map<V>(&mut self, _name: &str, _variant: &str, _visitor: V) -> Result<(), Error>
        where V: ser::MapVisitor,
    {
        unimplemented!();
    }

    #[inline]
    fn visit_map_elt<K, V>(&mut self, key: K, value: V) -> Result<(), Error>
        where K: ser::Serialize,
              V: ser::Serialize,
    {
        let leaf = self.expr.len() == 1;
        let head = self.expr.last().unwrap().clone();
        match head {
            SubExpr::Field(FieldExpr(ref field)) => {
                let mut ss = StringSerializer::new();
                try!(key.serialize(&mut ss));
                if field == ss.value.as_ref().unwrap() {
                    if leaf {
                        let mut ss = StringSerializer::new();
                        try!(value.serialize(&mut ss));
                        self.value = ss.value;
                        Ok(())
                    } else {
                        self.expr.pop();
                        value.serialize(self)
                    }
                } else {
                    Ok(())
                }
            },
            SubExpr::Index(_) => Err(Error::NotAnArray),
            SubExpr::Identity(_) => Err(Error::InternalError)
        }
    }

}



struct StringSerializer {
    value: Option<String>,
}

impl StringSerializer {
    /// Return a new string serializer
    fn new() -> StringSerializer {
        StringSerializer {
            value: None
        }
    }
}

impl ser::Serializer for StringSerializer {
    type Error = Error;

    fn visit_bool(&mut self, v: bool) -> Result<(), Error> {
        self.value = Some(v.to_string());
        Ok(())
    }

    fn visit_i64(&mut self, _v: i64) -> Result<(), Error> {
        unimplemented!()
    }

    fn visit_u64(&mut self, _v: u64) -> Result<(), Error> {
        unimplemented!()
    }

    fn visit_u32(&mut self, v: u32) -> Result<(), Error> {
        self.value = Some(v.to_string());
        Ok(())
    }

    fn visit_f64(&mut self, _v: f64) -> Result<(), Error> {
        unimplemented!()
    }

    fn visit_unit(&mut self) -> Result<(), Error> {
        unimplemented!()
    }

    fn visit_none(&mut self) -> Result<(), Error> {
        self.value = Some("EMPTY_TAG".to_string());
        Ok(())
    }

    fn visit_some<V>(&mut self, value: V) -> Result<(), Error>
        where V: ser::Serialize
    {
        value.serialize(self)
    }

    fn visit_seq<V>(&mut self, _visitor: V) -> Result<(), Error>
        where V: ser::SeqVisitor
    {
        unimplemented!()
    }

    fn visit_seq_elt<T>(&mut self,
                        _value: T) -> Result<(), Error>
        where T: ser::Serialize
    {
        unimplemented!()
    }

    fn visit_map<V>(&mut self, mut visitor: V) -> Result<(), Error>
        where V: ser::MapVisitor
    {
        // TODO: FIXME
        while let Some(()) = try!(visitor.visit(self)) { };
        Ok(())
    }

    fn visit_map_elt<K, V>(&mut self,
                           key: K,
                           _value: V) -> Result<(), Error>
        where K: ser::Serialize,
    V: ser::Serialize
    {
        key.serialize(self)
        // value.serialize(self)
    }

    #[inline]
    fn visit_str(&mut self, value: &str) -> Result<(), Error> {
        self.value = Some(value.into());
        Ok(())
    }
}

/// Filter the specified struct into a Value.
#[inline]
pub fn filter<'v, T>(value: &'v T, expr: Expr) -> Result<Option<String>, Error>
    where T: ser::Serialize
{
    let mut ser = Serializer::new(expr);
    try!(value.serialize(&mut ser));
    Ok(ser.value)
}


#[cfg(test)]
mod tests {
    // use std::boxed::Box;
    use std::collections::HashMap;

    use super::*;
    use ::expr::*;

    #[derive(Clone, Debug, PartialEq, Serialize)]
    struct S {
        a: String,
        b: u32
    }

    #[derive(Clone, Debug, PartialEq, Serialize)]
    struct T {
        s: S,
    }

    #[derive(Debug, PartialEq, Serialize)]
    struct U {
        t: T,
    }

    #[derive(Debug, PartialEq, Serialize)]
    struct H {
        m: HashMap<String, String>,
    }



    // Tests

    /// Extract a single field
    #[test]
    fn simple_expr_test () {
        let s = S {
            a: "I'm an a".to_string(),
            b: 0
        };

        let expr_a = Expr(vec![SubExpr::Field(FieldExpr("a".to_string()))]);
        let v = filter(&s, expr_a).unwrap().unwrap();
        assert_eq!(s.a, v);

        let expr_b = Expr(vec![SubExpr::Field(FieldExpr("b".to_string()))]);
        let v = filter(&s, expr_b).unwrap().unwrap();
        assert_eq!(s.b.to_string(), v);
    }

    /// Extract a nested field
    #[test]
    fn pipe_expr_test () {
        let t = T {
            s: S {
                a: "I'm an a".to_string(),
                b: 0
            }};

        let expr_a = Expr(vec![SubExpr::Field(FieldExpr("s".to_string())),
                               SubExpr::Field(FieldExpr("a".to_string()))]);
        let v = filter(&t, expr_a).unwrap().unwrap();
        assert_eq!(t.s.a, v);


        let u = U {
            t: T {
                s: S {
                    a: "I'm an a".to_string(),
                    b: 0
                }}
        };

        let expr_a = Expr(vec![SubExpr::Field(FieldExpr("t".to_string())),
                               SubExpr::Field(FieldExpr("s".to_string())),
                               SubExpr::Field(FieldExpr("a".to_string()))]);
        let v = filter(&u, expr_a).unwrap().unwrap();
        assert_eq!(u.t.s.a, v);
    }

    /// Extract a single field from a hash map
    #[test]
    fn hash_map_test () {
        let mut h = H {
            m: HashMap::new()
        };

        h.m.insert("a".to_string(), "x".to_string());
        h.m.insert("b".to_string(), "y".to_string());

        let expr_a = Expr(vec![SubExpr::Field(FieldExpr("m".to_string())),
                               SubExpr::Field(FieldExpr("a".to_string()))]);
        let v = filter(&h, expr_a).unwrap().unwrap();
        assert_eq!(*h.m.get("a").unwrap(), v);

        let expr_b = Expr(vec![SubExpr::Field(FieldExpr("m".to_string())),
                               SubExpr::Field(FieldExpr("c".to_string()))]);
        let v = filter(&h, expr_b).unwrap();
        assert!(v.is_none());
    }
}
