//! Error type for rq
use std::convert::From;
use std::error;
use std::fmt::{self, Display};
use parser_combinators::ParseError;

/// Map key type
pub type Key = String;

/// Expression error type
#[derive(Debug, PartialEq)]
pub enum Error{
    UnknownField(Key),
    NotAnArray,
    NotAssociative,
    NotCompound,
    InternalError,
    ParseError(ParseError)
}

impl From<ParseError> for Error {
    fn from(err: ParseError) -> Error {
        Error::ParseError(err)
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        match *self {
            Error::ParseError(ref e) => write!(f,"{}",e),
            Error::UnknownField(ref key) => write!(f, "UnknownField: {}", key),
            Error::NotAnArray => write!(f, "Not an array"),
            Error::NotAssociative => write!(f, "Not associative"),
            Error::NotCompound => write!(f, "Not compound"),
            Error::InternalError => write!(f, "Internal error"),
        }
    }
}

impl error::Error for Error {
    fn description(&self) -> &str {
        match *self {
            Error::ParseError(ref e) => e.description(),
            Error::UnknownField(_) => "Unknown field",
            Error::NotAnArray => "Not an array",
            Error::NotAssociative => "Not associative",
            Error::NotCompound => "Not compound",
            Error::InternalError => "Internal error",
        }

    }

    fn cause(&self) -> Option<&error::Error> {
        match *self {
            Error::ParseError(ref e) => Some(e),
            _ => None,
        }
    }
}
