//! Parser for filter expressions

use parser_combinators::*;
use parser_combinators::primitives::{State, Stream};

use ::error::*;
use ::expr::*;

fn identity_string((c, s) : (char, String)) -> String
{
    format!("{}{}", c, s)
}

fn identity_expr(_: char) -> SubExpr {
    SubExpr::Identity(IdentityExpr)
}

fn field_expr(s: String) -> SubExpr {
    SubExpr::Field(FieldExpr(s))
}

fn is_dot(c: char) -> bool {
    c == '.'
}

fn is_alphabetic_or_underscore(c: char) -> bool {
    c.is_alphabetic() || c == '_'
}

pub fn parse(input: &str) -> Result<Expr, Error> {

    fn identifier<I>(input: State<I>) -> ParseResult<String, I>
        where I: Stream<Item=char> {
            let mut id = letter()
                .and(many(satisfy(is_alphabetic_or_underscore)))
                .map(identity_string);
            id.parse_state(input)
        }

    fn sub_expr<I>(input: State<I>) -> ParseResult<SubExpr, I>
        where I: Stream<Item=char>
    {
        let field =
            satisfy(is_dot)
            .with(parser(identifier).map(field_expr));
        let identity = satisfy(is_dot).map(identity_expr);
        let mut sub_expr = field.or(identity);
        sub_expr.parse_state(input)
    }

    let mut expr = sep_by(parser(sub_expr), satisfy(|c| c == '|'));

    // let mut function = (identifier as fn(_) -> _)
    //     .and(between(string("("), string(")"),
    //                  (expr as fn(_) -> _)));

    // let mut top_expr = (expr as fn(_) -> _).or((function as fn(_) -> _));

    let (e,_) = try!(expr.parse(input));
    Ok(Expr(e))
}


#[cfg(test)]
mod tests {
    use super::*;
    use ::filter::*;

    #[derive(Debug, PartialEq, Serialize)]
    struct S {
        a: String,
        b: u32
    }

    #[derive(Debug, PartialEq, Serialize)]
    struct T {
        s: S,
    }

    #[test]
    fn simple_test () {
        let s = S { a: "I'm an a".to_string(), b: 0 };
        let input = ".a";
        let expr = parse(input).unwrap();
        assert_eq!(
            "I'm an a".to_string(),
            filter(&s, expr).unwrap().unwrap())
    }

    #[test]
    fn nested_test () {
        let t = T { s: S { a: "I'm an a".to_string(), b: 0 } };
        let input = ".s|.a";
        let expr = parse(input).unwrap();
        assert_eq!(
            "I'm an a".to_string(),
            filter(&t, expr).unwrap().unwrap())
    }

    // #[test]
    // fn function_test () {
    //     let t = T { s: S { a: "I'm an a".to_string(), b: 0 } };
    //     let input = "filter(.s, .b == 0)";
    //     let expr = parse(input).unwrap();
    //     assert_eq!(
    //         "I'm an a".to_string(),
    //         filter(&t, expr).unwrap())
    // }
}
